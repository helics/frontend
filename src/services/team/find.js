import Vue from 'vue';
import teamTransformer from './../../transformers/team';
import store from './../../store';
import memberService from './../member';
import teamService from '@/services/team';
import medalService from '@/services/medal';

async function fetch(teamId) {
  const resultTeam = await Vue.$http.get(`/api/teams/${teamId}`);
  const result = await memberService.find(teamId);
  const resultProfile = await teamService.findProfile(teamId);
  const resultMedals = await medalService.find(teamId);
  // if (result.membro.length === 0) result.membro = undefined;
  store.dispatch('team/store', teamTransformer.fetch(
    resultTeam.data,
    result.membro,
    result.administrador,
    resultProfile[0],
    resultMedals.medals,
  ));
}

export default function (team) {
  return fetch(team);
}
