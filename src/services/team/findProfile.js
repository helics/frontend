import Vue from 'vue';

async function fetch(teamId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/profile`);
  return response.data;
}

export default function (teamId) {
  return fetch(teamId);
}
