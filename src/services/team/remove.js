import Vue from 'vue';

async function remove(teamId) {
  const response = await Vue.$http.delete(`/api/teams/${teamId}`);
  if (response.status === 403) {
    return { error: 'Você precisa ser administrador para realizar esta ação.' };
  } else if (response.status !== 204) {
    return { error: 'Não foi possível realizar a operação.' };
  }
  return 'Time excluído!';
}

export default function (teamId) {
  return remove(teamId);
}
