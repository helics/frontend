import Vue from 'vue';

async function uploadImage(image, teamId) {
  const data = new FormData();
  data.append('teamimg', image.files[0]);
  const response = await Vue.$http.post(`/api/teams/${teamId}/img`, data, { headers: { 'Content-Type': 'multipart/form-data' } });
  if (response.status !== 200) {
    return { error: 'Não foi possível atualizar a imagem. Verifique se o formato é JPG/JPEG.' };
  }
  return response.data;
}

export default function (image, teamId) {
  return uploadImage(image, teamId);
}
