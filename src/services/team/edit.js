import Vue from 'vue';
import teamService from './../team';

async function edit(team, profile) {
  const response = await Vue.$http.put(`/api/teams/${team.id}`, team);
  if (response.status !== 200) {
    return { error: response.data };
  }
  teamService.find(team.id, profile);
  return 'Time alterado com sucesso!';
}

export default function (team, profile) {
  return edit(team, profile);
}
