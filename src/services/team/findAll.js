import Vue from 'vue';
import teamService from '@/services/team';

async function fetchTeams() {
  let data = '';
  const response = await Vue.$http.get('/api/teams');
  if (!response.data.errors) {
    if (response.status !== 404) {
      await Promise.all(response.data.map(async (team) => {
        const result = await teamService.findProfile(team.id);
        team.profile = result[0].profile;
        if (team.profile !== undefined) team.profile.memberId = result[0].id;
      }));
      data = response.data;
    } else {
      data = [];
    }
  } else {
    data = response.data.errors;
  }
  return data;
}

export default function () {
  return fetchTeams();
}
