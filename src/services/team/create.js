import Vue from 'vue';
import accountService from '@/services/account';

async function create(team) {
  const resposta = {};
  let responseImage = '';
  let uploadedImage = '';
  if (team.image) {
    responseImage = await accountService.uploadImage(team.image);
    uploadedImage = responseImage;
  }
  if (responseImage.error) {
    resposta.message = responseImage.error;
  } else {
    team.image = uploadedImage;
    await Vue.$http.post('/api/teams', team)
      .then(async (response) => {
        resposta.code = response.status;
        if (resposta.code === 500) {
          resposta.message = 'Erro ao executar a operação!';
        } else if (resposta.code === 201) {
          resposta.message = 'Time criado com sucesso!';
        } else {
          resposta.message = response.data.errors;
        }
      });
  }
  return resposta;
}

export default function (team) {
  return create(team);
}
