import find from './find';
import findAll from './findAll';
import findScore from './findScore';
import findProfile from './findProfile';
import uploadImage from './uploadImage';
import create from './create';
import edit from './edit';
import remove from './remove';

export default {
  find,
  findAll,
  findScore,
  findProfile,
  uploadImage,
  create,
  edit,
  remove,
};
