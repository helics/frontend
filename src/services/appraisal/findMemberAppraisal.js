import Vue from 'vue';

async function fetch(teamId, memberId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/members/${memberId}/appraisals`);
  if (response.status !== 200) {
    Vue.router.push({
      name: 'home.index',
    });
  }
  return response.data;
}

export default function (teamId, memberId) {
  return fetch(teamId, memberId);
}
