import Vue from 'vue';

async function create(teamId, appraisal) {
  const response = await Vue.$http.post(`/api/teams/${teamId}/appraisals`, appraisal);
  if (response.status !== 200) {
    return { error: 'Não foi possível realizar a operação' };
  }
  return 'Avaliação enviada!';
}

export default function (teamId, appraisal) {
  return create(teamId, appraisal);
}
