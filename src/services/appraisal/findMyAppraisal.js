import Vue from 'vue';

async function fetch(teamId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/appraisals`);
  if (response.status !== 200) {
    // Vue.router.push({
    //   name: 'home.index',
    // });
    return '';
  }
  return response.data;
}

export default function (teamId) {
  return fetch(teamId);
}
