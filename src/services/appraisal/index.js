import find from './find';
import findMemberAppraisal from './findMemberAppraisal';
import findMyAppraisal from './findMyAppraisal';
import findRanking from './findRanking';
import create from './create';

export default {
  find,
  findMemberAppraisal,
  findMyAppraisal,
  findRanking,
  create,
};
