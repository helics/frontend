import Vue from 'vue';

async function fetchRanking(teamId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/rankings/appraisal`);
  if (response.status !== 200) {
    Vue.router.push({
      name: 'home.index',
    });
  }
  return response.data;
}

export default function (teamId) {
  return fetchRanking(teamId);
}
