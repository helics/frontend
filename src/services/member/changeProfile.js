import Vue from 'vue';

async function remove(memberId, isAdmin) {
  let idProfile = '';
  if (isAdmin) idProfile = 1;
  else idProfile = 2;
  const response = await Vue.$http.put(`/api/members/${memberId}`, { profile: { id: idProfile } });
  if (response.status !== 200) {
    return { error: response.data };
  }
  if (idProfile === 1) return 'Membro marcado como administrador!';
  return 'Membro não é mais administrador do time!';
}

export default function (memberId, isAdmin) {
  return remove(memberId, isAdmin);
}
