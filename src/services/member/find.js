import Vue from 'vue';
// import accountService from '@/services/account';

async function fetch(id) {
  const response = await Vue.$http.get(`/api/teams/${id}/members`);
  if (response.status !== 200) {
    Vue.router.push({
      name: 'home.index',
    });
  }
  // if (response.data.membro !== undefined) {
    // await Promise.all(response.data.membro.map(async (member) => {
      // const image = await accountService.findImage(member.account.id);
      // member.account.image = image;
    // }));
  //   return response.data;
  // }
  return response.data;
}

export default function (id) {
  return fetch(id);
}
