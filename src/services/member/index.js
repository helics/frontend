import find from './find';
import changeProfile from './changeProfile';
import remove from './remove';

export default {
  find,
  remove,
  changeProfile,
};
