import Vue from 'vue';

async function remove(memberId) {
  const response = await Vue.$http.delete(`/api/members/${memberId}`);
  if (response.status === 403) {
    return { error: 'Você é o único administrador do time. Defina outro membro como administrador.' };
  } else if (response.status !== 204) {
    return { error: 'Não foi possível realizar a operação' };
  }
  return 'Membro foi removido do time!';
}

export default function (memberId) {
  return remove(memberId);
}
