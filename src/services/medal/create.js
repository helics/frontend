import Vue from 'vue';
import medalService from '@/services/medal';
import store from './../../store';

async function create(teamId, medal) {
  const response = await Vue.$http.post(`/api/teams/${teamId}/medals`, medal);
  if (response.status !== 201) {
    return { error: 'Não foi possível realizar a operação' };
  }
  const result = await medalService.find(teamId);
  store.dispatch('team/storemedal', result.medals);
  return 'Selo criado com sucesso!';
}

export default function (teamId, medal) {
  return create(teamId, medal);
}
