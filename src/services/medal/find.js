import Vue from 'vue';

async function fetch(teamId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/medals`);
  if (response.status !== 200) {
    Vue.router.push({
      name: 'home.index',
    });
  }
  return response.data;
}

export default function (teamId) {
  return fetch(teamId);
}
