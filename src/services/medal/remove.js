import Vue from 'vue';
import medalService from '@/services/medal';
import store from './../../store';

async function remove(teamId, medalId) {
  const response = await Vue.$http.delete(`/api/teams/${teamId}/medals/${medalId}`);
  if (response.status !== 204) {
    return { error: 'Não foi possível realizar a operação' };
  }
  const result = await medalService.find(teamId);
  store.dispatch('team/storemedal', result.medals);
  return 'Selo removido!';
}

export default function (teamId, medal) {
  return remove(teamId, medal);
}
