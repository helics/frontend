import find from './find';
import create from './create';
import edit from './edit';
import remove from './remove';

export default {
  find,
  create,
  edit,
  remove,
};
