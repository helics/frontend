import Vue from 'vue';
import store from './../../store';

export default () => {
  store.dispatch('auth/logout');
  Vue.router.go({
    name: 'login.index',
  });
};
