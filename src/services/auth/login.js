import Vue from 'vue';
import accountService from './../account';
import store from './../../store';

const success = (token) => {
  store.dispatch('auth/login', token);
  accountService.find();
  Vue.router.push({
    name: 'home.index',
  });
};

async function login(user) {
  let erro = '';
  await Vue.$http.post('/api/auth', user)
    .then((response) => {
      if (response.data.token) {
        success(response.data.token);
      } else if (response.status !== 500) {
        erro = response.data.errors;
      } else {
        erro = 'Erro ao conectar ao servidor.';
      }
    });
  return erro;
}

export default function (user) {
  return login(user);
}
