import Vue from 'vue';
import authService from '@/services/auth';
import accountService from './../account';

async function register(user) {
  let notificacao = '';
  let response = '';
  let uploadedImage = '';
  if (user.image) {
    response = await accountService.uploadImage(user.image);
    uploadedImage = response;
  }
  if (response.error) {
    notificacao = response.error;
  } else {
    user.image = uploadedImage;
    response = await Vue.$http.post('/api/accounts', user);
    if (!response.data.errors) {
      await authService.login(user);
    } else {
      notificacao = response.data.errors;
    }
  }
  return notificacao;
}

export default function (user) {
  return register(user);
}
