import login from './login';
import logout from './logout';
import register from './register';
import registerWithTeam from './registerWithTeam';

export default {
  login,
  logout,
  register,
  registerWithTeam,
};
