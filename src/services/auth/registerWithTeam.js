import Vue from 'vue';
import authService from '@/services/auth';
import accountService from './../account';

async function register(team) {
  let notificacao = '';
  let responseImageTeam = '';
  let responseImageAccount = '';
  let response = '';
  if (team.admin.image) {
    responseImageAccount = await accountService.uploadImage(team.admin.image);
  }
  if (responseImageAccount.error) {
    notificacao = responseImageAccount.error;
  } else {
    team.admin.image = responseImageAccount;
    if (team.image) {
      responseImageTeam = await accountService.uploadImage(team.image);
      if (responseImageTeam.error) {
        notificacao = responseImageTeam.error;
      } else {
        team.image = responseImageTeam;
        response = Vue.$http.post('/api/teams', team);
        if (!response.error) await authService.login(team.admin);
        else notificacao = response.error;
      }
    }
  }
  return notificacao;
}

export default function (team) {
  return register(team);
}
