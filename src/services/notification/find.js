import Vue from 'vue';

async function fetch(page) {
  const response = await Vue.$http.get(`/api/notifications?page=${page}`);
  return response.data;
}

export default function (page) {
  return fetch(page);
}
