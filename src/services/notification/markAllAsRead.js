import Vue from 'vue';

async function fetch() {
  const response = await Vue.$http.post('api/notifications/read');
  return response.data;
}

export default function () {
  return fetch();
}
