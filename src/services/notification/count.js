import Vue from 'vue';

async function count() {
  const response = await Vue.$http.get('/api/notifications/news');
  return response.data;
}

export default function () {
  return count();
}
