import find from './find';
import count from './count';
import markAllAsRead from './markAllAsRead';

export default {
  find,
  count,
  markAllAsRead,
};
