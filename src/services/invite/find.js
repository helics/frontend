import Vue from 'vue';
// import teamService from '@/services/team';

async function fetch() {
  let data = { error: 'Não foi possivel realizar a operação' };
  await Vue.$http.get('/api/invitations')
    .then(async (response) => {
      if (response.status === 200) {
        data = response.data;
        // await Promise.all(data.map(async (team) => {
          // const image = await teamService.findImage(team.team.id);
          // team.team.image = image;
        // }));
      }
    });
  return data;
}

export default async function () {
  return fetch();
}
