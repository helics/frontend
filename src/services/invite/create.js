import Vue from 'vue';

async function createInvite(teamId, email) {
  let data = 'Não foi possivel realizar a operação';
  await Vue.$http.post(`/api/invitations/teams/${teamId}`, email)
    .then((response) => {
      if (response.status === 201) {
        data = 'Convite enviado com sucesso!';
      } else if (response.status !== 500) {
        data = response.data;
      }
    });
  return data;
}

export default async function (teamId, email) {
  return createInvite(teamId, email);
}
