import Vue from 'vue';
import accountService from '@/services/account';

async function accept(inviteId) {
  let data = 'Não foi possivel realizar a operação';
  await Vue.$http.post(`/api/invitations/${inviteId}/accept`)
    .then((response) => {
      if (response.status === 200) {
        data = `Convite aceito. Você está agora no time ${response.data.team.name}`;
        accountService.find();
      }
    });
  return data;
}

export default async function (inviteId) {
  return accept(inviteId);
}
