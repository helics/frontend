import Vue from 'vue';
import accountService from '@/services/account';

async function reject(inviteId) {
  let data = 'Não foi possivel realizar a operação';
  await Vue.$http.delete(`/api/invitations/${inviteId}`)
    .then((response) => {
      if (response.status === 204) {
        data = 'Convite cancelado.';
        accountService.find();
      }
    });
  return data;
}

export default async function (inviteId) {
  return reject(inviteId);
}
