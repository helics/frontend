import Vue from 'vue';

async function fetch(teamId) {
  let data = { error: 'Não foi possivel realizar a operação' };
  await Vue.$http.get(`/api/teams/${teamId}/invitations`)
    .then((response) => {
      if (response.status === 200) {
        data = response.data;
      }
    });
  return data;
}

export default async function (teamId) {
  return fetch(teamId);
}
