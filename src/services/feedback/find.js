import Vue from 'vue';

async function fetch(teamId, page, option) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/feedback/received?page=${page}&option=${option}`);
  return response.data;
}

export default function (teamId, page, option) {
  return fetch(teamId, page, option);
}
