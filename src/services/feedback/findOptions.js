import Vue from 'vue';

async function fetch() {
  const response = await Vue.$http.get('/api/feedback/options');
  return response.data;
}

export default function () {
  return fetch();
}
