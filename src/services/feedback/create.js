import Vue from 'vue';

async function create(teamId, feedback) {
  const response = await Vue.$http.post(`/api/teams/${teamId}/feedback`, feedback);
  if (response.status !== 201) {
    return { error: 'Não foi possível realizar a operação' };
  }
  return 'Feedback enviado com sucesso!';
}

export default function (teamId, feedback) {
  return create(teamId, feedback);
}
