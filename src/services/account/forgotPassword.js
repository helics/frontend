import Vue from 'vue';

async function sendEmail(accountEmail) {
  const response = await Vue.$http.post('/api/auth/forgot', { email: accountEmail });
  if (response.status !== 200) {
    return { error: 'Não foi possível realizar a operação. Tente novamente.' };
  }
  return 'Verifique o endereço de email informado para redefinir sua conta.';
}

export default function (accountEmail) {
  return sendEmail(accountEmail);
}
