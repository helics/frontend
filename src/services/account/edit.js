import Vue from 'vue';

async function edit(account) {
  const response = await Vue.$http.put('/api/accounts', account);
  if (response.status !== 200) {
    return { error: response.data };
  }
  return 'Conta alterada com sucesso!';
}

export default function (account) {
  return edit(account);
}
