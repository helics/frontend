import find from './find';
import edit from './edit';
import forgotPassword from './forgotPassword';
import resetPassword from './resetPassword';
import uploadImage from './uploadImage';

export default {
  find,
  edit,
  forgotPassword,
  resetPassword,
  uploadImage,
};
