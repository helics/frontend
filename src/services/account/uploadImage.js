import Vue from 'vue';

async function uploadImage(image) {
  const data = new FormData();
  data.append('image', image.files[0]);
  const response = await Vue.$http.post('/api/images', data, { headers: { 'Content-Type': 'multipart/form-data' } });
  if (response.status !== 201) {
    return { error: 'Não foi possível atualizar a imagem. Verifique se o formato é JPEG.' };
  }
  return response.data;
}

export default function (image) {
  return uploadImage(image);
}
