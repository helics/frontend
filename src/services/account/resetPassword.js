import Vue from 'vue';

async function sendEmail(account) {
  const response = await Vue.$http.post('/api/auth/reset', account);
  if (response.status !== 200) {
    return { error: 'Não foi possível realizar a operação. Tente novamente.' };
  }
  return 'Senha alterada. Você será redirecionado para página de login.';
}

export default function (account) {
  return sendEmail(account);
}
