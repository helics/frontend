import Vue from 'vue';
import teamService from '@/services/team';
import store from './../../store';
import accountTransformer from './../../transformers/account';

let errors = '';

async function fetch() {
  let data = '';
  await Vue.$http.post('/api/auth/account')
    .then((response) => {
      if (!response.data.errors) {
        data = response.data;
      } else {
        errors = response.data.errors;
      }
    });
  return data;
}

export default async () => {
  let account = '';
  account = await fetch();
  account.teams = await teamService.findAll();
  store.dispatch('account/store', accountTransformer.fetch(account));
  return errors;
};
