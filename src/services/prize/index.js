import create from './create';
import find from './find';
import findRanking from './findRanking';

export default {
  create,
  find,
  findRanking,
};
