import Vue from 'vue';

async function fetchRanking(teamId) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/rankings/prize`);
  return response.data;
}

export default function (teamId) {
  return fetchRanking(teamId);
}
