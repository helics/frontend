import Vue from 'vue';

async function fetch(teamId, page) {
  const response = await Vue.$http.get(`/api/teams/${teamId}/prizes?page=${page}`);
  if (response.status !== 200) {
    Vue.router.push({
      name: 'home.index',
    });
  }
  return response.data;
}

export default function (teamId, page) {
  return fetch(teamId, page);
}
