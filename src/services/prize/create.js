import Vue from 'vue';

async function fetch(teamId, memberId, medalId, comment) {
  const response = await Vue.$http.post(`/api/teams/${teamId}/members/${memberId}/medals/${medalId}`, comment);
  if (response.status !== 201) {
    return { error: 'Não foi possível realizar a operação' };
  }
  return 'Prêmio atribuído com sucesso!';
}

export default function (teamId, memberId, medalId, commentInput) {
  return fetch(teamId, memberId, medalId, { comment: commentInput });
}
