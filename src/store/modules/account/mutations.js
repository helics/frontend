import { STORE } from './mutation-types';

export default {
  [STORE](state, account) {
    state.id = account.id;
    state.image = account.image;
    state.email = account.email;
    state.name = account.name;
    state.teams = account.teams;
  },
};
