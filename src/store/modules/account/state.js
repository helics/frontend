export default {
  id: null,
  image: null,
  name: null,
  email: null,
  teams: null,
};
