import * as types from './mutation-types';

export const store = ({ commit }, payload) => {
  commit(types.STORE, payload);
};

export const storemedal = ({ commit }, payload) => {
  commit(types.STOREMEDAL, payload);
};

export const clear = ({ commit }, payload) => {
  commit(types.CLEAR, payload);
};

export default {
  store,
  storemedal,
  clear,
};
