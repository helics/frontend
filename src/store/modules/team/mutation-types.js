export const STORE = 'STORE';
export const STOREMEDAL = 'STOREMEDAL';
export const CLEAR = 'CLEAR';

export default {
  STORE,
  STOREMEDAL,
  CLEAR,
};
