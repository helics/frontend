export default {
  id: null,
  name: null,
  description: null,
  admins: null,
  members: null,
  profile: null,
  image: null,
  medals: null,
};
