import { STORE, STOREMEDAL, CLEAR } from './mutation-types';

export default {
  [STORE](state, team) {
    state.id = team.id;
    state.name = team.name;
    state.description = team.description;
    state.admins = team.admins;
    state.members = team.members;
    state.profile = team.profile;
    state.image = team.image;
    state.medals = team.medals;
  },
  [STOREMEDAL](state, medals) {
    state.medals = medals;
  },
  [CLEAR](state) {
    state.id = '';
    state.name = '';
    state.description = '';
    state.admins = '';
    state.members = '';
    state.profile = '';
    state.image = '';
    state.medals = '';
  },
};
