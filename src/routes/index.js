export default [
  {
    path: '/home',
    name: 'home.index',
    component: require('@/pages/home/index.vue'),
    meta: {
      auth: true,
    },
  },

  {
    path: '/notifications',
    name: 'notifications.index',
    component: require('@/pages/notification/index.vue'),
    meta: {
      auth: true,
    },
  },

  {
    path: '/dashboard/team/:id',
    name: 'dashboard.index',
    component: require('@/pages/dashboard/index.vue'),
    meta: {
      auth: true,
    },
  },

  {
    path: '/account',
    name: 'account.index',
    component: require('@/pages/account/index.vue'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/resetPassword',
    name: 'reset.index',
    component: require('@/pages/reset/index.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/login',
    name: 'login.index',
    component: require('@/pages/login/index.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/register',
    name: 'register.index',
    component: require('@/pages/register/index.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/*',
    redirect: '/home',
  },
];
