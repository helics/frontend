import Vue from 'vue';

Vue.config.debug = process.env.NODE_ENV !== 'production';

import VueMaterial from 'vue-material';

Vue.use(VueMaterial);
Vue.material.registerTheme('default', {
  primary: 'indigo',
  accent: 'deep-orange',
  warn: 'red',
  background: {
    color: 'blue-grey',
    hue: 'A200',
  },
});
Vue.material.registerTheme('card', {
  primary: 'indigo',
  accent: 'deep-orange',
  warn: 'red',
  background: 'white',
});
Vue.material.registerTheme('radio', {
  primary: 'green',
  accent: 'orange',
  warn: 'red',
  background: 'grey',
});
require('vue-material/dist/vue-material.css');
// require('vue-multiselect/dist/vue-multiselect.min.css');

import Axios from 'axios';
import authService from '@/services/auth';

Axios.defaults.baseURL = process.env.API_LOCATION;
Axios.defaults.headers.common.Accept = 'application/json';
Axios.interceptors.response.use(
  response => response,
  (error) => {
    if (error.response.status === 401) {
      authService.logout();
    }
    return error.response;
  });

Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return Axios;
  },
});

import VuexRouterSync from 'vuex-router-sync';
import store from './store';

store.dispatch('auth/check');

import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

export const router = new VueRouter({
  routes,
  mode: 'history',
});
router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.auth) && !store.state.auth.authenticated) {
    next({
      name: 'login.index',
    });
  } else if (to.matched.some(m => m.meta.guest) && store.state.auth.authenticated) {
    /*
     * If the user is authenticated and visits
     * an guest page, redirect to the dashboard page
     */
    next({
      name: 'home.index',
    });
  } else {
    next();
  }
});
VuexRouterSync.sync(store, router);

Vue.router = router;

import jQuery from 'jquery';

window.$ = window.jQuery = jQuery;

require('bootstrap');
require('bootstrap/less/bootstrap.less');

// require('font-awesome/less/font-awesome.less');

export default {
  router,
};
