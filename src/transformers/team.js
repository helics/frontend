import Transformer from './transformer';

export default class TeamTransformer extends Transformer {
  static fetch(team, members, admins, profile, medals) {
    if (admins !== undefined) {
      if (members === undefined) members = [];
      members = admins.concat(members);
    }
    return {
      id: team.id,
      name: team.name,
      description: team.description,
      admins,
      members,
      profile: profile.profile,
      image: team.image,
      medals,
    };
  }
}
