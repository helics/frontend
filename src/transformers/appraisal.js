import Transformer from './transformer';

export default class AppraisalTransformer extends Transformer {

  static create(appraisal, member, chosenValue) {
    return {
      criteria: { id: Number(appraisal.id) },
      value: Number(chosenValue),
      evaluated: { id: Number(member.id) },
    };
  }

}
